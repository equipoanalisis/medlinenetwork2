package com.example.josejoescobar.medlinenetwork2;

public class DatosDoctores {

    private int id;
    private String Titulo;
    private String Descripcion;
    private int Imagen;


    /**
     * Constructor
     * @param id
     * @param titulo
     * @param descripcion
     * @param imagen
     */
    public DatosDoctores(int id, String titulo, String descripcion, int imagen) {
        this.id = id;
        Titulo = titulo;
        Descripcion = descripcion;
        Imagen = imagen;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return Titulo;
    }

    public void setTitulo(String titulo) {
        Titulo = titulo;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public int getImagen() {
        return Imagen;
    }

    public void setImagen(int imagen) {
        Imagen = imagen;
    }
}
