package com.example.josejoescobar.medlinenetwork2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class Adaptador extends BaseAdapter{

    Context contexto;
    List<DatosDoctores> ListaObjetos;


    public Adaptador(Context contexto, List<DatosDoctores> listaObjetos) {
        this.contexto = contexto;
        ListaObjetos = listaObjetos;
    }

    @Override
    public int getCount() {
        return ListaObjetos.size(); //retorna la cnatidad de elementos de la lista
    }

    @Override
    public Object getItem(int position) {
        return ListaObjetos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return ListaObjetos.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vista = convertView;
        LayoutInflater inflate = LayoutInflater.from(contexto);
        vista = inflate.inflate(R.layout.itemlistview,null);

        CircleImageView imagen = (CircleImageView) vista.findViewById(R.id.FotoDoctor1);
        TextView titulo = (TextView) vista.findViewById(R.id.txtTitulo1);
        TextView descripcion = (TextView) vista.findViewById(R.id.txtDescrip1);

        titulo.setText(ListaObjetos.get(position).getTitulo().toString());
        descripcion.setText(ListaObjetos.get(position).getDescripcion().toString());
        imagen.setImageResource(ListaObjetos.get(position).getImagen());

        return vista;



    }
}
