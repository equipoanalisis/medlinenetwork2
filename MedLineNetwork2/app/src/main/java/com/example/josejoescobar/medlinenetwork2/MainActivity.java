package com.example.josejoescobar.medlinenetwork2;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hitomi.cmlibrary.CircleMenu;
import com.hitomi.cmlibrary.OnMenuSelectedListener;
import com.hitomi.cmlibrary.OnMenuStatusChangeListener;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    CircleMenu circleMenu;
    CoordinatorLayout mylogin;
    AnimationDrawable animationDrawable;
    LinearLayout  botonDoctores,pedirMedicina,mapas,historialMedico,salirLogin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mylogin = (CoordinatorLayout) findViewById(R.id.mylogin);
        botonDoctores = (LinearLayout) findViewById(R.id.botonDoctores);
        botonDoctores.setOnClickListener(this);

        pedirMedicina = (LinearLayout) findViewById(R.id.pedirMedicina);
        pedirMedicina.setOnClickListener(this);

        mapas = (LinearLayout) findViewById(R.id.mapas);
        mapas.setOnClickListener(this);

        historialMedico = (LinearLayout) findViewById(R.id.historialMedico);
        historialMedico.setOnClickListener(this);

        salirLogin = (LinearLayout) findViewById(R.id.salirLogin);
        salirLogin.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        switch( view.getId()){
            case R.id.botonDoctores:
                Intent intent = new Intent(MainActivity.this, contenedordoctores.class);
               //intent.putExtra("id", id);
                startActivity(intent);
                break;

            case R.id.pedirMedicina:
                Intent intent2 = new Intent(MainActivity.this, SolicitarMedicinas.class);
                //intent.putExtra("id", id);
                startActivity(intent2);
                break;

           case R.id.mapas:
                Intent intent3 = new Intent(MainActivity.this, MapsActivity.class);
                //intent.putExtra("id", id);
                startActivity(intent3);
                break;

            case R.id.historialMedico:
                Intent intent4 = new Intent(MainActivity.this, HistorialMedico.class);
                //intent.putExtra("id", id);
                startActivity(intent4);
                break;

            case R.id.salirLogin:
                Intent intent5 = new Intent(MainActivity.this, Login.class);
                //intent.putExtra("id", id);
                startActivity(intent5);
                break;
        }

    }
}
