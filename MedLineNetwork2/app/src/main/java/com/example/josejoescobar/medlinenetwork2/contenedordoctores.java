package com.example.josejoescobar.medlinenetwork2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

public class contenedordoctores extends AppCompatActivity {

    ListView listadatos;
    ArrayList<DatosDoctores> Lista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contenedordoctores);

        listadatos = (ListView) findViewById(R.id.lstDatos);

        Lista = new ArrayList<DatosDoctores>();

        Lista.add(new DatosDoctores(1,"Dr. Oscar Morales","Medico Internista y Neurologia",R.drawable.doctor2));
        Lista.add(new DatosDoctores(2,"Dr. Rodolfo Rossino Graci","Cardiologo",R.drawable.doctor1));
        Lista.add(new DatosDoctores(3,"Dr. Mario Roberto Bran García","Cirugía de Columna\n" +
                "Traumatología y Ortopedia\n" +
                "\n" +
                "Telefono: 22191581\n" +
                "Email: columnavertebran@gmail.com ",R.drawable.doctorfrancisco));
        Lista.add(new DatosDoctores(4,"Dr. Francisco Alberto Estrada Lainfiesta","Ginecología y Obstetricia",R.drawable.doctorlainfiesta));
        Lista.add(new DatosDoctores(5,"Dr. Byron Augusto Chacón Díaz","Ginecología y Obstetricia",R.drawable.fotodrchacon));
        Lista.add(new DatosDoctores(6,"Dr. Carlos Augusto Chua López","Medicina Infantil\n" +
                "Nutrición Materno-Infantil",R.drawable.fotodrchua));
        Lista.add(new DatosDoctores(7,"Dr. Sergio Leonel Ralón Carranza","Cáncer de Mama\n" +
                "Enfermedades Mamarias\n" +
                "Cirugía General",R.drawable.fotodrralon));
        Lista.add(new DatosDoctores(8,"Dr. Víctor Hugo González Azmitía","Ginecología y Obstetricia",R.drawable.fotodrvhga));
        Lista.add(new DatosDoctores(9,"Dra. Elizabeth Orellana Melendez","Patología",R.drawable.fotodraorellana));
        Lista.add(new DatosDoctores(10,"Dr. Raúl Eduardo Cruz Ramírez","Cirugía General",R.drawable.rauleduardocruzramirez));



        Adaptador miadaptador = new Adaptador(getApplicationContext(),Lista);

        listadatos.setAdapter(miadaptador);


    }
}
